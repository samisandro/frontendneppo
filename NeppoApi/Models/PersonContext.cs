﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace NeppoApi.Models
{
    public class PersonContext : DbContext
    {
        public PersonContext() : base("name=PersonContext") { }
        protected override void OnModelCreating(DbModelBuilder modelBuilder) { }
        public DbSet<Person> Person { get; set; }
    }
}