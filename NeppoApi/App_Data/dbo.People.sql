﻿CREATE TABLE [dbo].[People] (
    [Id]         INT            IDENTITY (1, 1) NOT NULL,
    [Name]       NVARCHAR (150) NOT NULL,
    [DocumentId] NVARCHAR (13)  NOT NULL,
    [BirthDate]  DATETIME       NOT NULL,
    [Gender]     NVARCHAR (1)   NOT NULL,
    [Address]    NVARCHAR (200) NOT NULL,
    CONSTRAINT [PK_dbo.People] PRIMARY KEY CLUSTERED ([Id] ASC)
);

