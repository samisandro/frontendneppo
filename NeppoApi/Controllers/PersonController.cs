﻿using NeppoApi.Models;
using NeppoApi.Repository;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Cors;

namespace NeppoApi.Controllers
{
    [EnableCors("*", "*", "*")]
    public class PersonController : ApiController
    {
        private PersonRepository _PersonRepository;
        private Person entity;
        public PersonRepository PersonRepository
        {
            get
            {
                if (_PersonRepository == null)
                    _PersonRepository = new PersonRepository();
                return _PersonRepository;
            }
            set { _PersonRepository = value; }
        }
        public HttpResponseMessage Get()
        {
            try
            {
                var response = new HttpResponseMessage(HttpStatusCode.OK);
                response.Content = new StringContent(JsonConvert.SerializeObject(PersonRepository.GetAll()));
                return response;
            }
            catch
            {
                return new HttpResponseMessage(HttpStatusCode.BadRequest);
            }
        }

        public HttpResponseMessage Get(int Id)
        {
            try
            {
                var response = new HttpResponseMessage(HttpStatusCode.OK);
                response.Content = new StringContent(JsonConvert.SerializeObject(PersonRepository.GetById(Id)));
                return response;
            }
            catch
            {
                return new HttpResponseMessage(HttpStatusCode.BadRequest);
            }
        }

        public HttpResponseMessage Post(Person p)
        {
            try
            {
                PersonRepository.Save(p);
                return new HttpResponseMessage(HttpStatusCode.OK);
            }
            catch
            {
                return new HttpResponseMessage(HttpStatusCode.BadRequest);
            }
        }

        public HttpResponseMessage Put(int Id, Person p)
        {
            try
            {
                PersonRepository.Update(Id, p);
                return new HttpResponseMessage(HttpStatusCode.OK);
            }
            catch
            {
                return new HttpResponseMessage(HttpStatusCode.BadRequest);
            }
        }

        public HttpResponseMessage Delete(int Id)
        {
            try
            {
                entity = PersonRepository.GetById(Id);
                PersonRepository.Delete(entity);
                return new HttpResponseMessage(HttpStatusCode.OK);
            }
            catch
            {
                return new HttpResponseMessage(HttpStatusCode.BadRequest);
            }
        }
    }
}
