﻿using NeppoApi.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace NeppoApi.Repository
{
    public class PersonRepository : BaseRepository
    {
        private Person p;
        public Person GetById(int Id)
        {
            return db.Person.FirstOrDefault(x => x.Id == Id);
        }

        public List<Person> GetAll()
        {
            return db.Person.OrderBy(x => x.Name).ToList();
        }

        public List<Person> GetByName(string Name)
        {
            return db.Person.Where(x => x.Name.Contains(Name)).OrderBy(x => x.Name).ToList();
        }

        public void Save(Person entity)
        {
            db.Person.Add(entity);
            db.SaveChanges();
        }

        public void Update(int Id, Person entity)
        {
            p = this.GetById(Id);
            p.Name = entity.Name;
            p.DocumentId = entity.DocumentId;
            p.BirthDate = entity.BirthDate;
            p.Gender = entity.Gender;            
            p.Address = entity.Address;
            db.Entry(p).State = EntityState.Modified;
            db.SaveChanges();
        }

        public void Delete(Person entity)
        {
            db.Person.Remove(entity);
            db.SaveChanges();
        }
    }
}